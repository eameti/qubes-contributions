### Step
# CLONE https://github.com/airbytehq/airbyte.git
# GO to airbyte-integrations/connectors
# COPY metadata.yaml in each source-<name>
# SAVE to corresponding metadata.yaml
### Result
# ├── source-github
# │ ├── metadata.yaml
# │ ├── source_definition.json
# │ └── source_definition_specification.json
# └── source-monday
#   ├── metadata.yaml
#   ├── source_definition.json
#   └── source_definition_specification.json


import os
from decouple import config
from git import Repo

CONNECTOR_DIR = config('CONNECTOR_DIR')

def _clone_airbyte_gitrepo(local_dir: str) -> None:
    airbyte_github = 'https://github.com/airbytehq/airbyte.git'
    
    try:
        print(f'Cloning repo {airbyte_github} to {local_dir}')
        Repo.clone_from(airbyte_github, local_dir)
    except Exception as e:
        print(f'ERROR: {e}')
        

def get_source_metadata(airbyte_connector_dir: str) -> None:
   
    for qube_key in next(os.walk(CONNECTOR_DIR))[1]:
        if qube_key.startswith('source-'):
            source_dir = f'{CONNECTOR_DIR}/{qube_key}'
            airbyte_source_dir = f'{airbyte_connector_dir}/{qube_key}'
            print(airbyte_source_dir)
            
            try:
                with open(f'{airbyte_source_dir}/metadata.yaml', 'r') as file:
                    contents = file.read()
                
                with open(f'{source_dir}/metadata.yaml', 'w') as file:
                    file.write(contents)
                    
            except Exception as e:
                print(f'ERROR-{qube_key}: {e}')

if __name__ == '__main__':
    airbyte_local_dir = 'airbyte-git'
    airbyte_connector_dir = f'{airbyte_local_dir}/airbyte-integrations/connectors'
    
    _clone_airbyte_gitrepo(airbyte_local_dir)
    get_source_metadata(airbyte_connector_dir)
