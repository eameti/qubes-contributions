### Step
# POST /source_definitions/list_latest
# SPLIT the output
# SAVE to corresponding source_definition.json
### Result
# ├── source-github
# │ ├── source_definition.json
# └── source-monday
#   ├── source_definition.json
# ...
import os
from decouple import config
import requests
import json


BASEURL = config('BASEURL')
FORMAT_INDENT = config('FORMAT_INDENT', cast=int)
CONNECTOR_DIR = config('CONNECTOR_DIR')

def get_source_definition() -> None:
    # call to airbyte source_definition
    source_definition_url = f'{BASEURL}/source_definitions/list_latest'
    response = requests.post(source_definition_url)
    data = response.json()
 
    # loop through items
    for src in data['sourceDefinitions']:
        
        # get name based on image
        new_name = src['dockerRepository'].split('/')[1] # farosai/airbyte-harness-source
        
        # fix some odd format e.g airbyte-harness-source
        if new_name.endswith('source'):
            new_name = 'source-' + '-'.join(new_name.split('-')[1:-1])
        
        # create corresponding directory
        connector_dir = f'{CONNECTOR_DIR}/{new_name}'
        try:
            print(f'Creating {connector_dir}')
            os.makedirs(connector_dir)
        except Exception as e:
            pass
            
        # save content to source_definition.json
        print(f'Saving data to {connector_dir}/source_definition.json')
        with open(f'{connector_dir}/source_definition.json', 'w') as file:
            json.dump(src, file, indent=FORMAT_INDENT)

        # cleanups
        # os.remove(f'{connector_dir}/source_definition.json')
        # os.rmdir(connector_dir)


if __name__ == '__main__':
    get_source_definition()
