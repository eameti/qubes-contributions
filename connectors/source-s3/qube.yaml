qubeKey: source-s3
name: source-s3
type: airbyte-source
id: source-s3
maturity: generally_available
ui:
  icon: <svg xmlns="http://www.w3.org/2000/svg" width="250" height="250" fill="none"><path
    fill="url(#a)" fill-rule="evenodd" d="M57.364 37.095 41.3 46.483v12.944L32 51.603
    55.673 194.13l.027.012C56.734 210.71 87.365 224 125 224c37.634 0 68.264-13.289
    69.3-29.857l.027-.014L218 51.604l-6.764 3.657v-5.364l-15.218-12.802-32.127-6.828-24.518-2.56h-26.209l-29.591
    2.56-26.21 6.828Z" clip-rule="evenodd"/><path fill="#F2B0A9" fill-rule="evenodd"
    d="M125 77.207c51.362 0 93-11.463 93-25.604C218 37.463 176.362 26 125 26S32 37.463
    32 51.603c0 14.14 41.638 25.604 93 25.604Zm0-4.19c42.958 0 77.782-9.587 77.782-21.414
    0-11.826-34.824-21.413-77.782-21.413s-77.782 9.587-77.782 21.413c0 11.827 34.824
    21.414 77.782 21.414Z" clip-rule="evenodd"/><defs><linearGradient id="a" x1="52.291"
    x2="195.173" y1="119.856" y2="119.856" gradientUnits="userSpaceOnUse"><stop stop-color="#8C3123"/><stop
    offset="1" stop-color="#E05243"/></linearGradient></defs></svg>
  shortDescription: file
  description: ''
  links:
  - linkName: Documentation
    url: https://docs.airbyte.com/integrations/sources/s3
properties:
  type: object
  title: Config
  required:
  - streams
  - bucket
  properties:
    bucket:
      type: string
      order: 0
      title: Bucket
      description: Name of the S3 bucket where the file(s) exist.
    format:
      type: object
      oneOf:
      - type: object
        title: CSV
        properties:
          encoding:
            type: string
            order: 4
            title: Encoding
            default: utf8
            description: The character encoding of the CSV data. Leave blank to default
              to <strong>UTF8</strong>. See <a href="https://docs.python.org/3/library/codecs.html#standard-encodings"
              target="_blank">list of python encodings</a> for allowable options.
          filetype:
            type: string
            const: csv
            title: Filetype
            default: csv
          delimiter:
            type: string
            order: 0
            title: Delimiter
            default: ','
            minLength: 1
            description: The character delimiting individual cells in the CSV data.
              This may only be a 1-character string. For tab-delimited data enter
              '\t'.
          block_size:
            type: integer
            order: 9
            title: Block Size
            default: 10000
            maximum: 2147483647
            minimum: 1
            description: The chunk size in bytes to process at a time in memory from
              each file. If your data is particularly wide and failing during schema
              detection, increasing this should solve it. Beware of raising this too
              high as you could hit OOM errors.
          quote_char:
            type: string
            order: 2
            title: Quote Character
            default: '"'
            description: The character used for quoting CSV values. To disallow quoting,
              make this field blank.
          escape_char:
            type: string
            order: 3
            title: Escape Character
            description: The character used for escaping special characters. To disallow
              escaping, leave this field blank.
          double_quote:
            type: boolean
            order: 5
            title: Double Quote
            default: true
            description: Whether two quotes in a quoted CSV value denote a single
              quote in the data.
          infer_datatypes:
            type: boolean
            order: 1
            title: Infer Datatypes
            default: true
            description: Configures whether a schema for the source should be inferred
              from the current data or not. If set to false and a custom schema is
              set, then the manually enforced schema is used. If a schema is not manually
              set, and this is set to false, then all fields will be read as strings
          advanced_options:
            type: string
            order: 8
            title: Advanced Options
            examples:
            - '{"column_names": ["column1", "column2"]}'
            description: Optionally add a valid JSON string here to provide additional
              <a href="https://arrow.apache.org/docs/python/generated/pyarrow.csv.ReadOptions.html#pyarrow.csv.ReadOptions"
              target="_blank">Pyarrow ReadOptions</a>. Specify 'column_names' here
              if your CSV doesn't have header, or if you want to use custom column
              names. 'block_size' and 'encoding' are already used above, specify them
              again here will override the values above.
          newlines_in_values:
            type: boolean
            order: 6
            title: Allow newlines in values
            default: false
            description: Whether newline characters are allowed in CSV values. Turning
              this on may affect performance. Leave blank to default to False.
          additional_reader_options:
            type: string
            order: 7
            title: Additional Reader Options
            examples:
            - '{"timestamp_parsers": ["%m/%d/%Y %H:%M", "%Y/%m/%d %H:%M"], "strings_can_be_null":
              true, "null_values": ["NA", "NULL"]}'
            description: Optionally add a valid JSON string here to provide additional
              options to the csv reader. Mappings must correspond to options <a href="https://arrow.apache.org/docs/python/generated/pyarrow.csv.ConvertOptions.html#pyarrow.csv.ConvertOptions"
              target="_blank">detailed here</a>. 'column_types' is used internally
              to handle schema so overriding that would likely cause problems.
        description: 'This connector utilises <a href="https: // arrow.apache.org/docs/python/generated/pyarrow.csv.open_csv.html"
          target="_blank">PyArrow (Apache Arrow)</a> for CSV parsing.'
      - type: object
        title: Parquet
        properties:
          columns:
            type: array
            items:
              type: string
            order: 0
            title: Selected Columns
            description: If you only want to sync a subset of the columns from the
              file(s), add the columns you want here as a comma-delimited list. Leave
              it empty to sync all columns.
          filetype:
            type: string
            const: parquet
            title: Filetype
            default: parquet
          batch_size:
            type: integer
            order: 1
            title: Record batch size
            default: 65536
            description: "Maximum number of records per batch read from the input\
              \ files. Batches may be smaller if there aren\u2019t enough rows in\
              \ the file. This option can help avoid out-of-memory errors if your\
              \ data is particularly wide."
          buffer_size:
            type: integer
            title: Buffer Size
            default: 2
            description: Perform read buffering when deserializing individual column
              chunks. By default every group column will be loaded fully to memory.
              This option can help avoid out-of-memory errors if your data is particularly
              wide.
        description: This connector utilises <a href="https://arrow.apache.org/docs/python/generated/pyarrow.parquet.ParquetFile.html"
          target="_blank">PyArrow (Apache Arrow)</a> for Parquet parsing.
      - type: object
        title: Avro
        properties:
          filetype:
            type: string
            const: avro
            title: Filetype
            default: avro
        description: This connector utilises <a href="https://fastavro.readthedocs.io/en/latest/"
          target="_blank">fastavro</a> for Avro parsing.
      - type: object
        title: Jsonl
        properties:
          filetype:
            type: string
            const: jsonl
            title: Filetype
            default: jsonl
          block_size:
            type: integer
            order: 2
            title: Block Size
            default: 0
            description: The chunk size in bytes to process at a time in memory from
              each file. If your data is particularly wide and failing during schema
              detection, increasing this should solve it. Beware of raising this too
              high as you could hit OOM errors.
          newlines_in_values:
            type: boolean
            order: 0
            title: Allow newlines in values
            default: false
            description: Whether newline characters are allowed in JSON values. Turning
              this on may affect performance. Leave blank to default to False.
          unexpected_field_behavior:
            enum:
            - ignore
            - infer
            - error
            order: 1
            title: Unexpected field behavior
            default: infer
            examples:
            - ignore
            - infer
            - error
            description: How JSON fields outside of explicit_schema (if given) are
              treated. Check <a href="https://arrow.apache.org/docs/python/generated/pyarrow.json.ParseOptions.html"
              target="_blank">PyArrow documentation</a> for details
        description: This connector uses <a href="https://arrow.apache.org/docs/python/json.html"
          target="_blank">PyArrow</a> for JSON Lines (jsonl) file parsing.
      order: 120
      title: File Format
      default: csv
      description: Deprecated and will be removed soon. Please do not use this field
        anymore and use streams.format instead. The format of the files you'd like
        to replicate
      airbyte_hidden: true
    schema:
      type: string
      order: 130
      title: Manually enforced data schema
      default: '{}'
      examples:
      - '{"column_1": "number", "column_2": "string", "column_3": "array", "column_4":
        "object", "column_5": "boolean"}'
      description: 'Deprecated and will be removed soon. Please do not use this field
        anymore and use streams.input_schema instead. Optionally provide a schema
        to enforce, as a valid JSON string. Ensure this is a mapping of <strong>{
        "column" : "type" }</strong>, where types are valid <a href="https://json-schema.org/understanding-json-schema/reference/type.html"
        target="_blank">JSON Schema datatypes</a>. Leave as {} to auto-infer the schema.'
      airbyte_hidden: true
    dataset:
      type: string
      order: 100
      title: Output Stream Name
      pattern: ^([A-Za-z0-9-_]+)$
      description: Deprecated and will be removed soon. Please do not use this field
        anymore and use streams.name instead. The name of the stream you would like
        this source to output. Can contain letters, numbers, or underscores.
      airbyte_hidden: true
    streams:
      type: array
      items:
        type: object
        title: FileBasedStreamConfig
        required:
        - name
        - format
        properties:
          name:
            type: string
            title: Name
            description: The name of the stream.
          globs:
            type: array
            items:
              type: string
            title: Globs
            description: The pattern used to specify which files should be selected
              from the file system. For more information on glob pattern matching
              look <a href="https://en.wikipedia.org/wiki/Glob_(programming)">here</a>.
          format:
            type: object
            oneOf:
            - type: object
              title: Avro Format
              properties:
                filetype:
                  type: string
                  const: avro
                  title: Filetype
                  default: avro
                double_as_string:
                  type: boolean
                  title: Convert Double Fields to Strings
                  default: false
                  description: Whether to convert double fields to strings. This is
                    recommended if you have decimal numbers with a high degree of
                    precision because there can be a loss precision when handling
                    floating point numbers.
            - type: object
              title: CSV Format
              properties:
                encoding:
                  type: string
                  title: Encoding
                  default: utf8
                  description: The character encoding of the CSV data. Leave blank
                    to default to <strong>UTF8</strong>. See <a href="https://docs.python.org/3/library/codecs.html#standard-encodings"
                    target="_blank">list of python encodings</a> for allowable options.
                filetype:
                  type: string
                  const: csv
                  title: Filetype
                  default: csv
                delimiter:
                  type: string
                  title: Delimiter
                  default: ','
                  description: The character delimiting individual cells in the CSV
                    data. This may only be a 1-character string. For tab-delimited
                    data enter '\t'.
                quote_char:
                  type: string
                  title: Quote Character
                  default: '"'
                  description: The character used for quoting CSV values. To disallow
                    quoting, make this field blank.
                escape_char:
                  type: string
                  title: Escape Character
                  description: The character used for escaping special characters.
                    To disallow escaping, leave this field blank.
                null_values:
                  type: array
                  items:
                    type: string
                  title: Null Values
                  default: []
                  description: A set of case-sensitive strings that should be interpreted
                    as null values. For example, if the value 'NA' should be interpreted
                    as null, enter 'NA' in this field.
                  uniqueItems: true
                true_values:
                  type: array
                  items:
                    type: string
                  title: True Values
                  default:
                  - y
                  - 'yes'
                  - t
                  - 'true'
                  - 'on'
                  - '1'
                  description: A set of case-sensitive strings that should be interpreted
                    as true values.
                  uniqueItems: true
                double_quote:
                  type: boolean
                  title: Double Quote
                  default: true
                  description: Whether two quotes in a quoted CSV value denote a single
                    quote in the data.
                false_values:
                  type: array
                  items:
                    type: string
                  title: False Values
                  default:
                  - n
                  - 'no'
                  - f
                  - 'false'
                  - 'off'
                  - '0'
                  description: A set of case-sensitive strings that should be interpreted
                    as false values.
                  uniqueItems: true
                inference_type:
                  enum:
                  - None
                  - Primitive Types Only
                  title: Inference Type
                  default: None
                  description: How to infer the types of the columns. If none, inference
                    default to strings.
                  airbyte_hidden: true
                header_definition:
                  type: object
                  oneOf:
                  - type: object
                    title: From CSV
                    properties:
                      header_definition_type:
                        type: string
                        const: From CSV
                        title: Header Definition Type
                        default: From CSV
                  - type: object
                    title: Autogenerated
                    properties:
                      header_definition_type:
                        type: string
                        const: Autogenerated
                        title: Header Definition Type
                        default: Autogenerated
                  - type: object
                    title: User Provided
                    required:
                    - column_names
                    properties:
                      column_names:
                        type: array
                        items:
                          type: string
                        title: Column Names
                        description: The column names that will be used while emitting
                          the CSV records
                      header_definition_type:
                        type: string
                        const: User Provided
                        title: Header Definition Type
                        default: User Provided
                  title: CSV Header Definition
                  default:
                    header_definition_type: From CSV
                  description: How headers will be defined. `User Provided` assumes
                    the CSV does not have a header row and uses the headers provided
                    and `Autogenerated` assumes the CSV does not have a header row
                    and the CDK will generate headers using for `f{i}` where `i` is
                    the index starting from 0. Else, the default behavior is to use
                    the header from the CSV file. If a user wants to autogenerate
                    or provide column names for a CSV having headers, they can skip
                    rows.
                strings_can_be_null:
                  type: boolean
                  title: Strings Can Be Null
                  default: true
                  description: Whether strings can be interpreted as null values.
                    If true, strings that match the null_values set will be interpreted
                    as null. If false, strings that match the null_values set will
                    be interpreted as the string itself.
                skip_rows_after_header:
                  type: integer
                  title: Skip Rows After Header
                  default: 0
                  description: The number of rows to skip after the header row.
                skip_rows_before_header:
                  type: integer
                  title: Skip Rows Before Header
                  default: 0
                  description: The number of rows to skip before the header row. For
                    example, if the header row is on the 3rd row, enter 2 in this
                    field.
            - type: object
              title: Jsonl Format
              properties:
                filetype:
                  type: string
                  const: jsonl
                  title: Filetype
                  default: jsonl
            - type: object
              title: Parquet Format
              properties:
                filetype:
                  type: string
                  const: parquet
                  title: Filetype
                  default: parquet
                decimal_as_float:
                  type: boolean
                  title: Convert Decimal Fields to Floats
                  default: false
                  description: Whether to convert decimal fields to floats. There
                    is a loss of precision when converting decimals to floats, so
                    this is not recommended.
            - type: object
              title: Document File Type Format (Experimental)
              properties:
                filetype:
                  type: string
                  const: unstructured
                  title: Filetype
                  default: unstructured
              description: Extract text from document formats (.pdf, .docx, .md, .pptx)
                and emit as one record per file.
            title: Format
            description: The configuration options that are used to alter how to read
              incoming files that deviate from the standard formatting.
          schemaless:
            type: boolean
            title: Schemaless
            default: false
            description: When enabled, syncs will not validate or structure records
              against the stream's schema.
          primary_key:
            type: string
            title: Primary Key
            description: The column or columns (for a composite key) that serves as
              the unique identifier of a record.
          input_schema:
            type: string
            title: Input Schema
            description: The schema that will be used to validate records extracted
              from the file. This will override the stream schema that is auto-detected
              from incoming files.
          legacy_prefix:
            type: string
            title: Legacy Prefix
            description: The path prefix configured in v3 versions of the S3 connector.
              This option is deprecated in favor of a single glob.
            airbyte_hidden: true
          validation_policy:
            enum:
            - Emit Record
            - Skip Record
            - Wait for Discover
            title: Validation Policy
            default: Emit Record
            description: The name of the validation policy that dictates sync behavior
              when a record does not adhere to the stream schema.
          days_to_sync_if_history_is_full:
            type: integer
            title: Days To Sync If History Is Full
            default: 3
            description: When the state history of the file store is full, syncs will
              only read files that were last modified in the provided day range.
      order: 10
      title: The list of streams to sync
      description: Each instance of this configuration defines a <a href="https://docs.airbyte.com/cloud/core-concepts#stream">stream</a>.
        Use this to define which files belong in the stream, their format, and how
        they should be parsed and validated. When sending data to warehouse destination
        such as Snowflake or BigQuery, each stream is a separate table.
    endpoint:
      type: string
      order: 4
      title: Endpoint
      default: ''
      description: Endpoint to an S3 compatible service. Leave empty to use AWS.
    provider:
      type: object
      order: 111
      title: 'S3: Amazon Web Services'
      required: []
      properties:
        bucket:
          type: string
          order: 0
          title: Bucket
          description: Name of the S3 bucket where the file(s) exist.
        endpoint:
          type: string
          order: 4
          title: Endpoint
          default: ''
          description: Endpoint to an S3 compatible service. Leave empty to use AWS.
        start_date:
          type: string
          order: 5
          title: Start Date
          format: date-time
          pattern: ^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z$
          examples:
          - '2021-01-01T00:00:00Z'
          description: UTC date and time in the format 2017-01-25T00:00:00Z. Any file
            modified before this date will not be replicated.
        path_prefix:
          type: string
          order: 3
          title: Path Prefix
          default: ''
          description: By providing a path-like prefix (e.g. myFolder/thisTable/)
            under which all the relevant files sit, we can optimize finding these
            in S3. This is optional but recommended if your bucket contains many folders/files
            which you don't need to replicate.
        aws_access_key_id:
          type: string
          order: 1
          title: AWS Access Key ID
          always_show: true
          description: In order to access private Buckets stored on AWS S3, this connector
            requires credentials with the proper permissions. If accessing publicly
            available data, this field is not necessary.
          airbyte_secret: true
        aws_secret_access_key:
          type: string
          order: 2
          title: AWS Secret Access Key
          always_show: true
          description: In order to access private Buckets stored on AWS S3, this connector
            requires credentials with the proper permissions. If accessing publicly
            available data, this field is not necessary.
          airbyte_secret: true
      description: Deprecated and will be removed soon. Please do not use this field
        anymore and use bucket, aws_access_key_id, aws_secret_access_key and endpoint
        instead. Use this to load files from S3 or S3-compatible services
      airbyte_hidden: true
    start_date:
      type: string
      order: 1
      title: Start Date
      format: date-time
      pattern: ^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{6}Z$
      examples:
      - '2021-01-01T00:00:00.000000Z'
      description: UTC date and time in the format 2017-01-25T00:00:00.000000Z. Any
        file modified before this date will not be replicated.
      pattern_descriptor: YYYY-MM-DDTHH:mm:ss.SSSSSSZ
    path_pattern:
      type: string
      order: 110
      title: Pattern of files to replicate
      examples:
      - '**'
      - myFolder/myTableFiles/*.csv|myFolder/myOtherTableFiles/*.csv
      description: Deprecated and will be removed soon. Please do not use this field
        anymore and use streams.globs instead. A regular expression which tells the
        connector which files to replicate. All files which match this pattern will
        be replicated. Use | to separate multiple patterns. See <a href="https://facelessuser.github.io/wcmatch/glob/"
        target="_blank">this page</a> to understand pattern syntax (GLOBSTAR and SPLIT
        flags are enabled). Use pattern <strong>**</strong> to pick up all files.
      airbyte_hidden: true
    aws_access_key_id:
      type: string
      order: 2
      title: AWS Access Key ID
      description: In order to access private Buckets stored on AWS S3, this connector
        requires credentials with the proper permissions. If accessing publicly available
        data, this field is not necessary.
      airbyte_secret: true
    aws_secret_access_key:
      type: string
      order: 3
      title: AWS Secret Access Key
      description: In order to access private Buckets stored on AWS S3, this connector
        requires credentials with the proper permissions. If accessing publicly available
        data, this field is not necessary.
      airbyte_secret: true
  description: 'NOTE: When this Spec is changed, legacy_config_transformer.py must
    also be modified to uptake the changes

    because it is responsible for converting legacy S3 v3 configs into v4 configs
    using the File-Based CDK.'
schemaType: {}
