{
  "sourceDefinitionId": "ce38aec4-5a77-439a-be29-9ca44fd4e811",
  "documentationUrl": "https://docs.airbyte.com/integrations/sources/gnews",
  "connectionSpecification": {
    "type": "object",
    "title": "Gnews Spec",
    "$schema": "http://json-schema.org/draft-07/schema#",
    "required": [
      "api_key",
      "query"
    ],
    "properties": {
      "in": {
        "type": "array",
        "items": {
          "enum": [
            "title",
            "description",
            "content"
          ],
          "type": "string"
        },
        "order": 4,
        "title": "In",
        "description": "This parameter allows you to choose in which attributes the keywords are searched. The attributes that can be set are title, description and content. It is possible to combine several attributes."
      },
      "query": {
        "type": "string",
        "order": 1,
        "title": "Query",
        "examples": [
          "Microsoft Windows 10",
          "Apple OR Microsoft",
          "Apple AND NOT iPhone",
          "(Windows 7) AND (Windows 10)",
          "Intel AND (i7 OR i9)"
        ],
        "description": "This parameter allows you to specify your search keywords to find the news articles you are looking for. The keywords will be used to return the most relevant articles. It is possible to use logical operators  with keywords. - Phrase Search Operator: This operator allows you to make an exact search. Keywords surrounded by \n  quotation marks are used to search for articles with the exact same keyword sequence. \n  For example the query: \"Apple iPhone\" will return articles matching at least once this sequence of keywords.\n- Logical AND Operator: This operator allows you to make sure that several keywords are all used in the article\n  search. By default the space character acts as an AND operator, it is possible to replace the space character \n  by AND to obtain the same result. For example the query: Apple Microsoft is equivalent to Apple AND Microsoft\n- Logical OR Operator: This operator allows you to retrieve articles matching the keyword a or the keyword b.\n  It is important to note that this operator has a higher precedence than the AND operator. For example the \n  query: Apple OR Microsoft will return all articles matching the keyword Apple as well as all articles matching \n  the keyword Microsoft\n- Logical NOT Operator: This operator allows you to remove from the results the articles corresponding to the\n  specified keywords. To use it, you need to add NOT in front of each word or phrase surrounded by quotes.\n  For example the query: Apple NOT iPhone will return all articles matching the keyword Apple but not the keyword\n  iPhone"
      },
      "sortby": {
        "enum": [
          "publishedAt",
          "relevance"
        ],
        "type": "string",
        "order": 7,
        "title": "Sort By",
        "description": "This parameter allows you to choose with which type of sorting the articles should be returned. Two values  are possible:\n  - publishedAt = sort by publication date, the articles with the most recent publication date are returned first\n  - relevance = sort by best match to keywords, the articles with the best match are returned first"
      },
      "api_key": {
        "type": "string",
        "order": 0,
        "title": "API Key",
        "description": "API Key",
        "airbyte_secret": true
      },
      "country": {
        "enum": [
          "au",
          "br",
          "ca",
          "cn",
          "eg",
          "fr",
          "de",
          "gr",
          "hk",
          "in",
          "ie",
          "il",
          "it",
          "jp",
          "nl",
          "no",
          "pk",
          "pe",
          "ph",
          "pt",
          "ro",
          "ru",
          "sg",
          "es",
          "se",
          "ch",
          "tw",
          "ua",
          "gb",
          "us"
        ],
        "type": "string",
        "order": 3,
        "title": "Country",
        "description": "This parameter allows you to specify the country where the news articles returned by the API were published, the contents of the articles are not necessarily related to the specified country. You have to set as value the 2 letters code of the country you want to filter."
      },
      "end_date": {
        "type": "string",
        "order": 6,
        "title": "End Date",
        "pattern": "^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$",
        "examples": [
          "2022-08-21 16:27:09"
        ],
        "description": "This parameter allows you to filter the articles that have a publication date smaller than or equal to the  specified value. The date must respect the following format: YYYY-MM-DD hh:mm:ss (in UTC)"
      },
      "language": {
        "enum": [
          "ar",
          "zh",
          "nl",
          "en",
          "fr",
          "de",
          "el",
          "he",
          "hi",
          "it",
          "ja",
          "ml",
          "mr",
          "no",
          "pt",
          "ro",
          "ru",
          "es",
          "sv",
          "ta",
          "te",
          "uk"
        ],
        "type": "string",
        "order": 2,
        "title": "Language",
        "decription": "This parameter allows you to specify the language of the news articles returned by the API.  You have to set as value the 2 letters code of the language you want to filter."
      },
      "nullable": {
        "type": "array",
        "items": {
          "enum": [
            "title",
            "description",
            "content"
          ],
          "type": "string"
        },
        "order": 5,
        "title": "Nullable",
        "description": "This parameter allows you to specify the attributes that you allow to return null values. The attributes that  can be set are title, description and content. It is possible to combine several attributes"
      },
      "start_date": {
        "type": "string",
        "order": 6,
        "title": "Start Date",
        "pattern": "^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$",
        "examples": [
          "2022-08-21 16:27:09"
        ],
        "description": "This parameter allows you to filter the articles that have a publication date greater than or equal to the  specified value. The date must respect the following format: YYYY-MM-DD hh:mm:ss (in UTC)"
      },
      "top_headlines_query": {
        "type": "string",
        "order": 8,
        "title": "Top Headlines Query",
        "examples": [
          "Microsoft Windows 10",
          "Apple OR Microsoft",
          "Apple AND NOT iPhone",
          "(Windows 7) AND (Windows 10)",
          "Intel AND (i7 OR i9)"
        ],
        "description": "This parameter allows you to specify your search keywords to find the news articles you are looking for. The keywords will be used to return the most relevant articles. It is possible to use logical operators  with keywords. - Phrase Search Operator: This operator allows you to make an exact search. Keywords surrounded by \n  quotation marks are used to search for articles with the exact same keyword sequence. \n  For example the query: \"Apple iPhone\" will return articles matching at least once this sequence of keywords.\n- Logical AND Operator: This operator allows you to make sure that several keywords are all used in the article\n  search. By default the space character acts as an AND operator, it is possible to replace the space character \n  by AND to obtain the same result. For example the query: Apple Microsoft is equivalent to Apple AND Microsoft\n- Logical OR Operator: This operator allows you to retrieve articles matching the keyword a or the keyword b.\n  It is important to note that this operator has a higher precedence than the AND operator. For example the \n  query: Apple OR Microsoft will return all articles matching the keyword Apple as well as all articles matching \n  the keyword Microsoft\n- Logical NOT Operator: This operator allows you to remove from the results the articles corresponding to the\n  specified keywords. To use it, you need to add NOT in front of each word or phrase surrounded by quotes.\n  For example the query: Apple NOT iPhone will return all articles matching the keyword Apple but not the keyword\n  iPhone"
      },
      "top_headlines_topic": {
        "enum": [
          "breaking-news",
          "world",
          "nation",
          "business",
          "technology",
          "entertainment",
          "sports",
          "science",
          "health"
        ],
        "type": "string",
        "order": 9,
        "title": "Top Headlines Topic",
        "description": "This parameter allows you to change the category for the request."
      }
    },
    "additionalProperties": true
  },
  "jobInfo": {
    "id": "6a36e648-4196-45b5-9384-aad203e39486",
    "configType": "get_spec",
    "configId": "Optional.empty",
    "createdAt": 1705523890436,
    "endedAt": 1705523890436,
    "succeeded": true,
    "connectorConfigurationUpdated": false,
    "logs": {
      "logLines": []
    }
  }
}