qubeKey: source-tidb
name: source-tidb
type: airbyte-source
id: source-tidb
maturity: alpha
ui:
  icon: <svg xmlns="http://www.w3.org/2000/svg" width="250" height="250" fill="none"><path
    fill="#E30C34" d="M32.463 71.56v107.119l92.766 53.55 92.766-53.55V71.559L125.229
    18 32.463 71.56Z"/><path fill="#fff" d="m125.173 53.755-61.61 35.571v35.572l30.82-17.795v71.615l30.799
    17.757V89.317l30.79-17.776-30.799-17.786Zm30.973 53.531v71.393l30.935-17.863V89.413l-30.935
    17.873Z"/></svg>
  shortDescription: database
  description: ''
  links:
  - linkName: Documentation
    url: https://docs.airbyte.com/integrations/sources/tidb
properties:
  type: object
  title: TiDB Source Spec
  schema: http://json-schema.org/draft-07/schema#
  required:
  - host
  - port
  - database
  - username
  properties:
    ssl:
      type: boolean
      order: 6
      title: SSL Connection
      default: false
      description: Encrypt data using SSL.
    host:
      type: string
      order: 0
      title: Host
      description: Hostname of the database.
    port:
      type: integer
      order: 1
      title: Port
      default: 4000
      maximum: 65536
      minimum: 0
      examples:
      - '4000'
      description: Port of the database.
    database:
      type: string
      order: 2
      title: Database
      description: Name of the database.
    password:
      type: string
      order: 4
      title: Password
      description: Password associated with the username.
      airbyte_secret: true
    username:
      type: string
      order: 3
      title: Username
      description: Username to use to access the database.
    tunnel_method:
      type: object
      oneOf:
      - title: No Tunnel
        required:
        - tunnel_method
        properties:
          tunnel_method:
            type: string
            const: NO_TUNNEL
            order: 0
            description: No ssh tunnel needed to connect to database
      - title: SSH Key Authentication
        required:
        - tunnel_method
        - tunnel_host
        - tunnel_port
        - tunnel_user
        - ssh_key
        properties:
          ssh_key:
            type: string
            order: 4
            title: SSH Private Key
            multiline: true
            description: OS-level user account ssh key credentials in RSA PEM format
              ( created with ssh-keygen -t rsa -m PEM -f myuser_rsa )
            airbyte_secret: true
          tunnel_host:
            type: string
            order: 1
            title: SSH Tunnel Jump Server Host
            description: Hostname of the jump server host that allows inbound ssh
              tunnel.
          tunnel_port:
            type: integer
            order: 2
            title: SSH Connection Port
            default: 22
            maximum: 65536
            minimum: 0
            examples:
            - '22'
            description: Port on the proxy/jump server that accepts inbound ssh connections.
          tunnel_user:
            type: string
            order: 3
            title: SSH Login Username
            description: OS-level username for logging into the jump server host.
          tunnel_method:
            type: string
            const: SSH_KEY_AUTH
            order: 0
            description: Connect through a jump server tunnel host using username
              and ssh key
      - title: Password Authentication
        required:
        - tunnel_method
        - tunnel_host
        - tunnel_port
        - tunnel_user
        - tunnel_user_password
        properties:
          tunnel_host:
            type: string
            order: 1
            title: SSH Tunnel Jump Server Host
            description: Hostname of the jump server host that allows inbound ssh
              tunnel.
          tunnel_port:
            type: integer
            order: 2
            title: SSH Connection Port
            default: 22
            maximum: 65536
            minimum: 0
            examples:
            - '22'
            description: Port on the proxy/jump server that accepts inbound ssh connections.
          tunnel_user:
            type: string
            order: 3
            title: SSH Login Username
            description: OS-level username for logging into the jump server host
          tunnel_method:
            type: string
            const: SSH_PASSWORD_AUTH
            order: 0
            description: Connect through a jump server tunnel host using username
              and password authentication
          tunnel_user_password:
            type: string
            order: 4
            title: Password
            description: OS-level password for logging into the jump server host
            airbyte_secret: true
      title: SSH Tunnel Method
      description: Whether to initiate an SSH tunnel before connecting to the database,
        and if so, which kind of authentication to use.
    jdbc_url_params:
      type: string
      order: 5
      title: JDBC URL Params
      description: 'Additional properties to pass to the JDBC URL string when connecting
        to the database formatted as ''key=value'' pairs separated by the symbol ''&''.
        (example: key1=value1&key2=value2&key3=value3)'
schemaType: {}
