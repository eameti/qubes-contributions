{
  "sourceDefinitionId": "c2281cee-86f9-4a86-bb48-d23286b4c7bd",
  "documentationUrl": "https://docs.airbyte.com/integrations/sources/slack",
  "connectionSpecification": {
    "type": "object",
    "title": "Slack Spec",
    "$schema": "http://json-schema.org/draft-07/schema#",
    "required": [
      "start_date",
      "lookback_window",
      "join_channels"
    ],
    "properties": {
      "start_date": {
        "type": "string",
        "title": "Start Date",
        "format": "date-time",
        "pattern": "^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z$",
        "examples": [
          "2017-01-25T00:00:00Z"
        ],
        "description": "UTC date and time in the format 2017-01-25T00:00:00Z. Any data before this date will not be replicated."
      },
      "credentials": {
        "type": "object",
        "oneOf": [
          {
            "type": "object",
            "order": 0,
            "title": "Sign in via Slack (OAuth)",
            "required": [
              "option_title",
              "client_id",
              "client_secret",
              "access_token"
            ],
            "properties": {
              "client_id": {
                "type": "string",
                "title": "Client ID",
                "description": "Slack client_id. See our <a href=\"https://docs.airbyte.com/integrations/sources/slack\">docs</a> if you need help finding this id."
              },
              "access_token": {
                "type": "string",
                "title": "Access token",
                "description": "Slack access_token. See our <a href=\"https://docs.airbyte.com/integrations/sources/slack\">docs</a> if you need help generating the token.",
                "airbyte_secret": true
              },
              "option_title": {
                "type": "string",
                "const": "Default OAuth2.0 authorization"
              },
              "client_secret": {
                "type": "string",
                "title": "Client Secret",
                "description": "Slack client_secret. See our <a href=\"https://docs.airbyte.com/integrations/sources/slack\">docs</a> if you need help finding this secret.",
                "airbyte_secret": true
              }
            }
          },
          {
            "type": "object",
            "order": 1,
            "title": "API Token",
            "required": [
              "option_title",
              "api_token"
            ],
            "properties": {
              "api_token": {
                "type": "string",
                "title": "API Token",
                "description": "A Slack bot token. See the <a href=\"https://docs.airbyte.com/integrations/sources/slack\">docs</a> for instructions on how to generate it.",
                "airbyte_secret": true
              },
              "option_title": {
                "type": "string",
                "const": "API Token Credentials"
              }
            }
          }
        ],
        "title": "Authentication mechanism",
        "description": "Choose how to authenticate into Slack"
      },
      "join_channels": {
        "type": "boolean",
        "title": "Join all channels",
        "default": true,
        "description": "Whether to join all channels or to sync data only from channels the bot is already in.  If false, you'll need to manually add the bot to all the channels from which you'd like to sync messages. "
      },
      "channel_filter": {
        "type": "array",
        "items": {
          "type": "string",
          "minLength": 0
        },
        "title": "Channel name filter",
        "default": [],
        "examples": [
          "channel_one",
          "channel_two"
        ],
        "description": "A channel name list (without leading '#' char) which limit the channels from which you'd like to sync. Empty list means no filter."
      },
      "lookback_window": {
        "type": "integer",
        "title": "Threads Lookback window (Days)",
        "default": 0,
        "maximum": 365,
        "minimum": 0,
        "examples": [
          7,
          14
        ],
        "description": "How far into the past to look for messages in threads, default is 0 days"
      }
    },
    "additionalProperties": true
  },
  "advancedAuth": {
    "authFlowType": "oauth2.0",
    "predicateKey": [
      "credentials",
      "option_title"
    ],
    "predicateValue": "Default OAuth2.0 authorization",
    "oauthConfigSpecification": {
      "completeOAuthOutputSpecification": {
        "type": "object",
        "properties": {
          "access_token": {
            "type": "string",
            "path_in_connector_config": [
              "credentials",
              "access_token"
            ]
          }
        },
        "additionalProperties": false
      },
      "completeOAuthServerInputSpecification": {
        "type": "object",
        "properties": {
          "client_id": {
            "type": "string"
          },
          "client_secret": {
            "type": "string"
          }
        },
        "additionalProperties": false
      },
      "completeOAuthServerOutputSpecification": {
        "type": "object",
        "properties": {
          "client_id": {
            "type": "string",
            "path_in_connector_config": [
              "credentials",
              "client_id"
            ]
          },
          "client_secret": {
            "type": "string",
            "path_in_connector_config": [
              "credentials",
              "client_secret"
            ]
          }
        },
        "additionalProperties": false
      }
    }
  },
  "jobInfo": {
    "id": "a6ee9b5a-d1c5-4944-a135-766d498e9216",
    "configType": "get_spec",
    "configId": "Optional.empty",
    "createdAt": 1705523897360,
    "endedAt": 1705523897360,
    "succeeded": true,
    "connectorConfigurationUpdated": false,
    "logs": {
      "logLines": []
    }
  }
}